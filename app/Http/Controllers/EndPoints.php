<?php

namespace App\Http\Controllers;

use App\Console\Commands\Parse;
use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\DoctorClinics;
use App\Models\DoctorSpecialities;
use App\Models\Review;
use App\Models\Speciality;
use Illuminate\Support\Facades\Request;

class EndPoints extends Controller
{
    public function getDoctors()
    {
        $result = [];
        $doctors = Doctor::all();

        ### берём только необходимые записи из справочников
        ## получаем связи докторов с клиниками и специализациями
        $doctor_clinics = DoctorClinics::whereIn('doctor_id', array_column($doctors->toArray(), 'id'))->get();
        $doctor_specializations = DoctorSpecialities::whereIn('doctor_id', array_column($doctors->toArray(), 'id'))->get();

        ## получаем клиники и специализации
        $clinics = Clinic::whereIn('id', array_column($doctor_clinics->toArray(), 'clinic_id'))->get();
        $specializations = Speciality::whereIn('id', array_column($doctor_specializations->toArray(), 'speciality_id'))->get();

        ### создаём справочники клиник и специализаций по каждому доктору
        $doctor_clinics_bucket = [];
        foreach ($doctor_clinics as $item) {
            if (!isset($doctor_clinics_bucket[$item->doctor_id])) {
                $doctor_clinics_bucket[$item->doctor_id] = [];
            }
            $doctor_clinics_bucket[$item->doctor_id][] = $item->clinic_id;
        }

        $doctor_specializations_bucket = [];
        foreach ($doctor_specializations as $item) {
            if (!isset($doctor_specializations_bucket[$item->doctor_id])) {
                $doctor_specializations_bucket[$item->doctor_id] = [];
            }
            $doctor_specializations_bucket[$item->doctor_id][] = $item->speciality_id;
        }

        ### добавляем к списку докторов полные данные о клиниках и специализациях
        foreach ($doctors as $doctor) {
            $data = $doctor->toArray();
            # избавляемся от ключей, чтобы получился индексированный массив
            $data['clinics'] = array_values(
                # берём список клиник для данного доктора
                array_filter($clinics->toArray(), function ($clinic) use ($doctor, $doctor_clinics_bucket) {
                    return in_array($clinic['id'], isset($doctor_clinics_bucket[$doctor->id]) ? $doctor_clinics_bucket[$doctor->id] : []);
                })
            );
            # избавляемся от ключей, чтобы получился индексированный массив
            $data['specializations'] = array_values(
            # берём список специализаций для данного доктора
                array_filter($specializations->toArray(), function ($specialization) use ($doctor, $doctor_specializations_bucket) {
                    return in_array($specialization['id'], isset($doctor_specializations_bucket[$doctor->id]) ?  $doctor_specializations_bucket[$doctor->id] : []);
                })
            );

            $result[] = $data;
        }

        return json_encode($result);
    }

    public function getClinics()
    {
        return Clinic::all()->toJson();
    }

    public function getSpecialites()
    {
        return Speciality::all()->toJson();
    }

    public function getReviews()
    {
        return Review::all()->toJson();
    }

    public function goParse()
    {
        $code = Request::get('code');

        if ($code !== 'qwaszx') {
            return 'access denied';
        }

        $cmd = new Parse();
        $cmd->handle();

        echo "<br><br>done";
    }
}
