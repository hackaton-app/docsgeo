<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Models\News;
use Carbon\Carbon;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class MainScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Все новости';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Отображаем новости из всех источников';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
        return [
            Link::make('Website')
                ->href('http://orchid.software')
                ->icon('globe-alt'),

            Link::make('Documentation')
                ->href('https://orchid.software/en/docs')
                ->icon('docs'),

            Link::make('GitHub')
                ->href('https://github.com/orchidsoftware/platform')
                ->icon('social-github'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $date2_days_ago = (new Carbon())->subDays(2)->toDateTimeString();


        $data = News::where('created_at', '>', $date2_days_ago)->get();

        $data = $data->toArray();

        usort($data, function($a, $b) {
            if ($a['datetime_millisecond'] > $b['datetime_millisecond'])
                return -1;
            elseif ($a['datetime_millisecond'] < $b['datetime_millisecond'])
                return 1;
            else
                return 0;
        });

        return [
            Layout::view('news-main-list', ['list' => $data]),
        ];
    }
}
