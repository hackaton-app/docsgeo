<?php
/**
 * Created by PhpStorm.
 * User: shubin.ma
 * Date: 04.10.2019
 * Time: 11:39
 */

namespace App\Parsers;

use App\Helpers\ParserHelper;
use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\DoctorClinics;
use App\Models\DoctorSpecialities;
use App\Models\Speciality;
use Drnxloc\LaravelHtmlDom\HtmlDomParser;

class parseDocOnline {

    private $main_url = 'https://doc.online/ge/doctors/batumi/all';

    public function getMainUrl()
    {
        return $this->main_url;
    }

    public function getResult()
    {
        return $this->parseStart();
    }

    private function parseStart()
    {
        echo "\r\n";
        echo "Start parser " . __CLASS__ . "\r\n";

        $return = [];
        $i = 0;

        while (1) {
            $i++;

            $section_url = $this->main_url . '/page-' . $i;
            echo "\r\nsection_url={$section_url}\r\n";

            try {
                $content = ParserHelper::getContent($section_url);
                $html = HtmlDomParser::str_get_html($content);
                $blocks = $html->find('#doctorListView .doctor-block');

                foreach ($blocks as $vcard) {
                    # Фамилия, имяЮ отчество. Сразу превращаем в массив с разделителем "пробел".
                    $full_name = explode(' ', $vcard->find('span', 0)->innertext);
                    # Специальность
                    $speciality = $vcard->find('.card__label__text', 0)->innertext;
                    # Название клиники
                    $clinic_name = $vcard->find('.small-card__title', 0)->innertext;
                    # Адрес
                    $address = $vcard->find('.card__address__btn', 0)->attr['data-address'];
                    # Телефон
                    $phone_number = $vcard->find('.visit-time_toggle-phone_link', 0)->attr['data-phone'];

                    echo $full_name[0] . "\r\n";
                    echo $full_name[1] . "\r\n";
                    echo $speciality . "\r\n";
                    echo $clinic_name . "\r\n";
                    echo $address . "\r\n";
                    echo $phone_number . "\r\n";

                    echo "=====\r\n";

                    # сохраняем специальность
                    $speciality_model = $this->findCreateSpeciality($speciality);
                    # сохраняем клинику
                    $clinic_model = $this->findCreateClinic($clinic_name, $address, $phone_number);

                    /** todo
                     * проверять на существование записи доктора, обновлять данные
                     */
                    # сохраняем доктора
                    $this->findCreateDoctor($full_name[0], $full_name[1], $speciality_model->id, $clinic_model->id);
                }
            } catch (\InvalidArgumentException $e) {
                echo $e->getMessage()
                . "\r\n"
                . $e->getFile()
                . "\r\n"
                . $e->getLine()
                . "\r\n";
                echo 'Finish';
                break;
            }
        }

        echo "End parser " . __CLASS__ . "\r\n";

        return $return;
    }

    private function findCreateSpeciality($title)
    {
        return Speciality::firstOrCreate([
            'name' => $title,
        ]);
    }

    private function findCreateClinic($title, $address, $phone_number)
    {
        return Clinic::firstOrCreate([
            'name' => $title,
            'address' => $address,
            'phone' => $phone_number,
        ]);
    }

    private function findCreateDoctor($surname, $name, $speciality_id, $clinic_id)
    {
        /** todo
         * проверять на существование записи доктора, обновлять данные
         */

        $doctor = Doctor::create([
            'name' => $name,
            'surname' => $surname,
            'photo' => '',
            'description' => '',
        ]);

        DoctorClinics::create([
            'doctor_id' => $doctor->id,
            'clinic_id' => $clinic_id,
        ]);

        DoctorSpecialities::create([
            'doctor_id' => $doctor->id,
            'speciality_id' => $speciality_id,
        ]);
    }
}
