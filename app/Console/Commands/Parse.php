<?php

namespace App\Console\Commands;

use App\Models\Doctor;
use App\Models\Review;
use App\Models\Speciality;
use Illuminate\Console\Command;

class Parse extends Command
{
    private $parsers_list = [
        'parseDocOnline',
    ];

    private $result = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docsgeo:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Сайты и разделы, откуда тянется инфо:<br>";

        foreach ($this->parsers_list as $parser_name) {
            $full_class_name = "\\App\\Parsers\\" . $parser_name;
            $parser = new $full_class_name();

            echo '<b>' . $parser->getMainUrl() . '</b>';

            $parser_result = 'error';

            try {
                $parser_result = $parser->getResult();
            } catch (\Exception $e) {
                echo "\r\n Error in {$parser_name}\r\n";
                echo $e->getMessage() . "\r\n\r\n";
            }

            if (is_string($parser_result) && $parser_result === 'error') {
                echo "\r\nCan't read from remote\r\n";
                continue;
            }
        }

        usort($this->result, function($a, $b) {
            if ($a['datetime_millisecond'] > $b['datetime_millisecond'])
                return -1;
            elseif ($a['datetime_millisecond'] < $b['datetime_millisecond'])
                return 1;
            else
                return 0;
        });

        return Command::SUCCESS;
    }
}
