<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorSpecialities extends Model
{
    use HasFactory;

    protected $fillable = [
        'doctor_id',
        'speciality_id',
    ];

    protected $table = 'doctor_specialities';
}
