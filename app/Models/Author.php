<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'is_blocked',
    ];

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
