<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'author_id',
        'doctor_id',
        'is_blocked',
        'rating',
        'text',
    ];

    public function author()
    {
        return $this->hasOne(Author::class);
    }
}
