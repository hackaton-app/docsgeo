<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $fillable= [
        'name',
        'surname',
        'photo',
        'description',
    ];

    public function clinics()
    {
        return $this->hasMany(Clinic::class);
    }

    public function specialites()
    {
        return $this->hasMany(Speciality::class);
    }
}
