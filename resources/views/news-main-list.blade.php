<div class="bg-white rounded-top shadow-sm mb-3">

    <div class="row g-0">
        <table class="news-list">

        <tr>
            <th>Источник</th>
            <th>Раздел</th>
            <th>Дата</th>
            <th>Заголовок</th>
            <th>Ссылка</th>
        </tr>
        @foreach ($list as $item)
            <tr>
                <td>{{$item['source']}}</td>
                <td style="white-space: nowrap">{{$item['section_title']}}</td>
                <td style="white-space: nowrap">{{$item['datetime']}}</td>
                <td>{{$item['title']}}</td>
                <td><a href="{{$item['link']}}" target="_blank">Link</a></td>
            </tr>
        @endforeach
        </table>
    </div>

<style>
    table.news-list tr {
        /*border-top: 1px solid #fff;*/
        border-bottom: 1px solid #fff;
    }
    table.news-list tr:hover {
        background: lightgoldenrodyellow;
        /*border-top: 1px solid grey;*/
        border-bottom: 1px solid grey;
    }
    table.news-list td {
        padding: 0 6px;
    }
</style>
</div>
