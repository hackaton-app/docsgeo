<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/doctors', 'App\Http\Controllers\EndPoints@getDoctors');
Route::get('/specializations', 'App\Http\Controllers\EndPoints@getSpecialites');
Route::get('/clinics', 'App\Http\Controllers\EndPoints@getClinics');
Route::get('/reviews', 'App\Http\Controllers\EndPoints@getReviews');

Route::get('/go_parse', 'App\Http\Controllers\EndPoints@goParse');
